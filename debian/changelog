user-session-migration (0.4.3) unstable; urgency=medium

  [ Guido Berhoerster ]
  * unit tests:
    - Fix compatibility with Python >= 3.12. (Closes: #1074645).

  [ Mike Gabriel ]
  * tests/:
    - Stop using nosetests3, use Python standard library's unittest module
      instead.
  * debian/control:
    + Drop from B-D: python3-nose. (Closes: #1071840).
    + Bump Standards-Version to 4.7.0. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 12 Aug 2024 09:44:58 +0200

user-session-migration (0.4.2) unstable; urgency=medium

  * CMakeLists.txt:
    - Modernize CMake, only require C compiler, define upstream version in
      CMake.
  * tests/debhelper_tests.py:
    - Ignore missing env variables DEB_BUILD_OPTIONS and DH_INTERNAL_OPTIONS.
  * tests/migration_tests.py:
    - RegExp-escape search strings. Don't fail on tests if build directory
      contains regexp-special-characters (such as '+') in the path name.
      (Closes: #1061084).
  * src/user-session-migration.c:
    - Fix FTBFS on architectures affected by t64 transition. (Closes: #1067203).
  * debian/control:
    + Bump Standards-Version: to 4.6.2. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 25 Mar 2024 12:17:55 +0100

user-session-migration (0.4.1) unstable; urgency=medium

  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sun, 09 Apr 2023 14:06:15 -0400

user-session-migration (0.4.0) experimental; urgency=medium

  * Initial release to Debian (Closes: #1013992)
  * Rename to user-session-migration per debian-devel feedback.
    This means the dh helper is now user-session-migration and you
    can Build-Depend on dh-sequence-user-session-migration to use it.
  * Drop obsolete upstart support; there's already a systemd user service
  * Update debian/copyright

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 05 Apr 2023 16:38:41 -0400

session-migration (0.3.6) jammy; urgency=medium

  * Have dh-migrations Provide dh-sequence-migrations
  * Build-Depend on debhelper-compat 13

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 17 Mar 2022 10:19:20 -0400

session-migration (0.3.5build1) impish; urgency=medium

  * No-change rebuild to build packages with zstd compression.

 -- Matthias Klose <doko@ubuntu.com>  Thu, 07 Oct 2021 12:24:13 +0200

session-migration (0.3.5) focal; urgency=medium

  * tests/debhelper_tests.py:
    - updated test_build_with_missing_script to the format changes in the
      debhelper error string, fixes the build (lp: #1870081)

 -- Sebastien Bacher <seb128@ubuntu.com>  Thu, 02 Apr 2020 15:18:57 +0200

session-migration (0.3.4) disco; urgency=medium

  * Fix failing tests due to keyfiles get from stdlib not keeping tuple
    order. (LP: #1823435)

 -- Didier Roche <didrocks@ubuntu.com>  Tue, 09 Apr 2019 15:38:19 +0200

session-migration (0.3.3) bionic; urgency=medium

  * src/session-migration.c:
    fix default permission when creating unexisting parent directories
    to be 700. (LP: #1735929)

 -- Didier Roche <didrocks@ubuntu.com>  Tue, 23 Jan 2018 10:31:30 +0100

session-migration (0.3.2) artful; urgency=medium

  * tests/debhelper_tests.py: Unset $DH_INTERNAL_OPTIONS from the environment
    when building packages for the tests. When we do an arch-only build, this
    contains '-a', which causes the test packages to not build since they are
    arch: any. (LP: #1704238)

 -- Iain Lane <iain.lane@canonical.com>  Fri, 14 Jul 2017 10:51:03 +0100

session-migration (0.3.1) artful; urgency=medium

  * src/session-migration.c: Set the locale, so we can get unicode output.
  * tests/migration_tests.py, debian/control, CMakeLists.txt: Expect “ and ”
    as quote marks from GLib - as of 2.51, this is what it outputs. Require
    this version of glib at build-time.
  * debian/rules: Run the tests in C.UTF-8 so we can actually get those
    characters.

 -- Iain Lane <iain.lane@canonical.com>  Thu, 13 Jul 2017 16:44:57 +0100

session-migration (0.3) yakkety; urgency=medium

  * Add systemd user units, which are used if the user session is launched
    under systemd.

 -- Iain Lane <iain.lane@canonical.com>  Fri, 22 Jul 2016 12:08:48 +0100

session-migration (0.2.3) wily; urgency=low

  * debian/patches/fix-typo-in-package-desc.dff: fix typo in package
    description. The word 'packaged' was replaced with 'packages'.
    (LP: #1193822)

 -- Gayan Weerakutti <gayan@reversiblean.com>  Wed, 06 May 2015 11:39:12 +0530

session-migration (0.2.2) vivid; urgency=medium

  [ Justin McPherson ]
  * Fix spelling in doc

 -- Sebastien Bacher <seb128@ubuntu.com>  Wed, 15 Apr 2015 12:37:15 +0200

session-migration (0.2.1) trusty; urgency=low

  [ Didier Roche ]
  * add more info to the manpage

  [ Iain Lane ]
  * Add an upstart user session script, so we can run session-migration under
    more types of session.

 -- Iain Lane <iain.lane@canonical.com>  Thu, 09 Jan 2014 11:27:26 +0000

session-migration (0.2) quantal; urgency=low

  * dh_migrations adds a dependency using ${misc:Depends} on the packages
    using a migration
  * add a second binary packages for test and making additional checks
  * fix a typo in the man

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 19 Jul 2012 16:50:49 +0200

session-migration (0.1) quantal; urgency=low

  * Initial release

 -- Didier Roche <didrocks@ubuntu.com>  Thu, 12 Jul 2012 18:41:19 +0200
